package be.capgemini;

public class Calculator {

	   private Addition addition;
	   
	   public Calculator(Addition addition) {
	      this.addition = addition;
	   }

	   public int add(int numberOne, int numberTwo) {
	      return addition.add(numberOne, numberTwo);
	   }
	}

