package be.capgemini;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class Addition {
	
	private static final Logger LOG = LoggerFactory.getLogger(Addition.class);

    public static void main(String... args){
        LOG.debug("This will be printed on debug");
        LOG.info("This will be printed on info");
        LOG.warn("This will be printed on warn");
        LOG.error("This will be printed on error");

        LOG.info("Appending string: {}.", "Hello, World");
    }
	
	public int add (Integer i, Integer j) {
	      
	      if( i == null || j == null) {
	         throw new IllegalArgumentException("you must pass non null values");
	      }
	      
	      return i + j;
	   }

	}

