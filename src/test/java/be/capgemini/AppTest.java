package be.capgemini;

import static org.assertj.core.api.Assertions.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
     
    @Test
   public void a_few_simple_assertions() {
      assertThat("The Lord of the Rings").isNotNull()   
                                         .startsWith("The") 
                                         .contains("Lord") 
                                         .endsWith("Rings"); 
    }
}
