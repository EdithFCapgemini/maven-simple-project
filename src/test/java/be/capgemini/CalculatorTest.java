package be.capgemini;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.Mockito;

public class CalculatorTest {

   @Test
   public void testAdd() {
      //setup
      Addition addition = Mockito.mock(Addition.class);
      Calculator calculator = new Calculator(addition);
      
      when(addition.add(Mockito.anyInt(), Mockito.anyInt())).thenReturn(3);
      
      //execute
      int result = calculator.add(1, 2);
      
      //verify
      assertThat(result).isEqualTo(3);
      
   
   }
   
   @Test
   public void testAddException() {
      //setup
      Addition addition = Mockito.mock(Addition.class);
      final Calculator calculator = new Calculator(addition);
      
      when(addition.add(Mockito.eq(1), Mockito.eq(2))).thenThrow(new IllegalArgumentException("NOPE"));
      
      assertThatThrownBy(() -> calculator.add(1, 2))
      					.isInstanceOf(IllegalArgumentException.class);
      
      
   
   }
}
